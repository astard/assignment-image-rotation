#include<bmp.h>
#include<file_operations.h>
#include<image_format.h>
#include<malloc.h>
#include<stdint.h>
#include<stdio.h>
#include<transformation.h>

const char* ERROR_TOO_FEW_ARGS = "Вы ввели неверное количество аргументов.";

static void print_error(const char* msg){
    fprintf(stderr, "%s\n", msg);
}

int main(int argc, char* argv[]){
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3){ //1 параметр - имя функции
        print_error(ERROR_TOO_FEW_ARGS);
        return 0;
    }
    const char* source_img_path = argv[1];
    const char* transformed_img_path = argv[2];
    const char* input_mode = "rb";
    const char* output_mode = "wb";
    FILE* input_file = NULL;
    FILE* output_file = NULL;

    enum open_status in_open_status = open_file(&input_file, source_img_path, input_mode);
    enum open_status out_open_status = open_file(&output_file, transformed_img_path, output_mode);
    if (in_open_status != OPEN_OK){
        print_error(open_status_msg[in_open_status]);
        return 1;
    }
    if (out_open_status != OPEN_OK){
        print_error(open_status_msg[out_open_status]);
        return 1;
    }

    struct image img = {0};
    enum read_status in_read_status = from_bmp(input_file, &img);
    if (in_read_status != READ_OK){
        print_error(read_status_msg[in_read_status]);
        close_file(input_file);
        close_file(output_file);
        free_img(&img);
        return 1;
    }

    struct image new_img = rotate(img);
    enum write_status out_write_status = to_bmp(output_file, &new_img);
    if (out_write_status != WRITE_OK){
        print_error(write_status_msg[out_write_status]); 
        close_file(input_file);
        close_file(output_file);
        free_img(&img);
        free_img(&new_img);
        return 1;
    }
    free_img(&img);
    free_img(&new_img);

    enum close_status in_close_status = close_file(input_file);
    enum close_status out_close_status = close_file(output_file);
    if (in_close_status != CLOSE_OK){
        print_error(close_status_msg[in_close_status]);
        return 1;
    }
    if (out_close_status != CLOSE_OK){
        print_error(close_status_msg[out_close_status]);
        return 1;
    }
    
    return 0;
}
