#include<image_format.h>

struct image img_create(uint32_t width, uint32_t height){
    return (struct image){
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    };
}

void free_img(struct image* img){
    img->width = 0;
    img->height = 0;
    if((img->data) != NULL) free(img->data);
}
