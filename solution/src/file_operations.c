#include<file_operations.h>

enum open_status open_file (FILE** file, const char* file_path, const char* mode){
    if (file_path == NULL) return OPEN_ERROR_NULL_FILE_PATH;
    *file = fopen(file_path, mode);
    if (*file == NULL) return OPEN_ERROR;
    return OPEN_OK;
}

enum close_status close_file(FILE* file) {
    if (file == NULL) return CLOSE_NULL_FILE;
    if(fclose(file)) return CLOSE_ERROR;
    return CLOSE_OK;
}
