#include <stddef.h>
#include<transformation.h>

static uint64_t x_offset(size_t j, uint64_t height) {
    return j * height;
}

static uint64_t y_offset (size_t i, uint64_t height){
    return height - 1 - i;
}

static uint64_t get_new_img_index (uint64_t height, size_t i, size_t j){
    return x_offset(j, height) + y_offset(i, height);
}

static uint64_t get_old_img_index(size_t i, size_t j, uint64_t width) {
    return i * width + j;
}

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source ){
    struct image new_img = img_create(source.height, source.width);
    for (size_t i = 0; i < source.height; i++){
        for (size_t j = 0; j < source.width; j++){
            new_img.data[get_new_img_index(source.height, i, j)] = source.data[get_old_img_index(i, j, source.width)];
        }
    }
    return new_img;
}
