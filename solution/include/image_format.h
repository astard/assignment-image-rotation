#ifndef IMAGE_FORMAT_H
#define IMAGE_FORMAT_H

#include<stdint.h>
#include<malloc.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image img_create(uint32_t width, uint32_t height);
void free_img(struct image* const img);

#endif
