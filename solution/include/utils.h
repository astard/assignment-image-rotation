#ifndef UTILS_H
#define UTILS_H

#include<image_format.h>
#include<malloc.h>
#include<stdint.h>

uint8_t calculate_padding(uint32_t width);
uint8_t* create_padding_arr(uint8_t padding);
void free_padding_arr(uint8_t* padding_arr);
uint32_t calculate_image_size(struct image* img, uint8_t padding);

#endif
