#ifndef FILE_OPERATIONS_H
#define FILE_OPERATIONS_H

#include<stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR_NULL_FILE_PATH,
    OPEN_ERROR
};

static const char* open_status_msg[] = {
    [OPEN_OK] = "Файл успешно открыт",
    [OPEN_ERROR_NULL_FILE_PATH] = "Для открытия файла был передан путь NULL",
    [OPEN_ERROR] = "Возникла ошибка при открытии файла"
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_NULL_FILE,
    CLOSE_ERROR
};

static const char* close_status_msg[] = {
    [CLOSE_OK] = "Файл успешно закрыт",
    [CLOSE_NULL_FILE] = "Для закрытия файла был передан путь NULL",
    [CLOSE_ERROR] = "Возникла ошибка при закрытии файла"
};

enum open_status open_file (FILE** file, const char* file_path, const char* mode);
enum close_status close_file(FILE* file);

#endif
