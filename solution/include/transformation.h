#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include<stdint.h>
#include<image_format.h>

struct image rotate( struct image const source );

#endif
