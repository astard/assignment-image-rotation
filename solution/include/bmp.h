#ifndef BMP_H
#define BMP_H

#include<stdio.h>
#include<image_format.h>
#include<utils.h>
#include<malloc.h>
#include <inttypes.h>
#include <stdbool.h>

enum read_status{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_NULL,
    READ_ERROR
};

static const char* read_status_msg[] = {
    [READ_OK] = "Файл успешно прочитан",
    [READ_INVALID_SIGNATURE] = "Тип файла в заголовке не поддерживается",
    [READ_INVALID_BITS] = "Данная глубина цвета не поддерживается",
    [READ_INVALID_HEADER] = "Прочитанный заголовок некорректен",
    [READ_ERROR] = "Ошибка во время прочтения файла",
    [READ_FILE_NULL] = "На чтение передан файл NULL"
};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_ERROR,
    WRITE_FILE_NULL,
    WRITE_IMG_NULL
};

static const char* write_status_msg[] = {
    [WRITE_OK] = "Файл успешно записан",
    [WRITE_HEADER_ERROR] = "Ошибка с записью заголовка",
    [WRITE_ERROR] = "Возникла ошибка при записи файла",
    [WRITE_FILE_NULL] = "На запись передан файл NULL",
    [WRITE_IMG_NULL] = "На запись передано NULL изображение"
};

enum read_status from_bmp (FILE* in, struct image* img);
enum write_status to_bmp (FILE* out, struct image* img);
#endif
